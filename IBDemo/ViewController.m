//
//  ViewController.m
//  IBDemo
//
//  Created by James Cash on 05-07-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "MyView.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet MyView *thePinkView;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *ourLabels;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"view is %@", self.thePinkView);
    NSLog(@"We have %ld labels connected", self.ourLabels.count);
    for (UILabel *lbl in self.ourLabels) {
        lbl.text = [lbl.text stringByAppendingString:@"!"];
    }

    //UIView *someView = [[NSBundle mainBundle] loadNibNamed:@"View" owner:nil options:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)somethingHappened:(UIButton *)sender {
    NSLog(@"Button was pressed");
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    return [identifier isEqualToString:@"greenSegue"];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"greenSegue"]) {
        NSLog(@"Sender is %@", sender);
        UIViewController *dvc = segue.destinationViewController;
        dvc.title = @"Green Controller";
    }
}

@end
